let http = require('http');
let fs = require('fs');
let url = require('url');

const EventEmitter = require('events');

let App = {
    start: function (port) {
        let myEmitter = new EventEmitter();
        let server = http.createServer((request, response) => {
            response.writeHead(200, {
                'Content-Type': 'text/html; charset:utf-8'
            });
            if(request.url == '/'){
                myEmitter.emit('root', response)
            }
            response.end();
        }).listen(port);
        return myEmitter;
    }
}

module.exports = App;